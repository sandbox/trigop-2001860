/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

(function ($) {
    Drupal.behaviors.ada = {
        attach: function (context, settings) {
            $('ul.sf-menu').superfish();
        }
    }
})(jQuery);
