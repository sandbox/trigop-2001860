<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * A QUICK OVERVIEW OF DRUPAL THEMING
 *
 *   The default HTML for all of Drupal's markup is specified by its modules.
 *   For example, the comment.module provides the default HTML markup and CSS
 *   styling that is wrapped around each comment. Fortunately, each piece of
 *   markup can optionally be overridden by the theme.
 *
 *   Drupal deals with each chunk of content using a "theme hook". The raw
 *   content is placed in PHP variables and passed through the theme hook, which
 *   can either be a template file (which you should already be familiary with)
 *   or a theme function. For example, the "comment" theme hook is implemented
 *   with a comment.tpl.php template file, but the "breadcrumb" theme hooks is
 *   implemented with a theme_breadcrumb() theme function. Regardless if the
 *   theme hook uses a template file or theme function, the template or function
 *   does the same kind of work; it takes the PHP variables passed to it and
 *   wraps the raw content with the desired HTML markup.
 *
 *   Most theme hooks are implemented with template files. Theme hooks that use
 *   theme functions do so for performance reasons - theme_field() is faster
 *   than a field.tpl.php - or for legacy reasons - theme_breadcrumb() has "been
 *   that way forever."
 *
 *   The variables used by theme functions or template files come from a handful
 *   of sources:
 *   - the contents of other theme hooks that have already been rendered into
 *     HTML. For example, the HTML from theme_breadcrumb() is put into the
 *     $breadcrumb variable of the page.tpl.php template file.
 *   - raw data provided directly by a module (often pulled from a database)
 *   - a "render element" provided directly by a module. A render element is a
 *     nested PHP array which contains both content and meta data with hints on
 *     how the content should be rendered. If a variable in a template file is a
 *     render element, it needs to be rendered with the render() function and
 *     then printed using:
 *       <?php print render($variable); ?>
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. With this file you can do three things:
 *   - Modify any theme hooks variables or add your own variables, using
 *     preprocess or process functions.
 *   - Override any theme function. That is, replace a module's default theme
 *     function with one you write.
 *   - Call hook_*_alter() functions which allow you to alter various parts of
 *     Drupal's internals, including the render elements in forms. The most
 *     useful of which include hook_form_alter(), hook_form_FORM_ID_alter(),
 *     and hook_page_alter(). See api.drupal.org for more information about
 *     _alter functions.
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   If a theme hook uses a theme function, Drupal will use the default theme
 *   function unless your theme overrides it. To override a theme function, you
 *   have to first find the theme function that generates the output. (The
 *   api.drupal.org website is a good place to find which file contains which
 *   function.) Then you can copy the original function in its entirety and
 *   paste it in this template.php file, changing the prefix from theme_ to
 *   ada_. For example:
 *
 *     original, found in modules/field/field.module: theme_field()
 *     theme override, found in template.php: ada_field()
 *
 *   where ada is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_field() function.
 *
 *   Note that base themes can also override theme functions. And those
 *   overrides will be used by sub-themes unless the sub-theme chooses to
 *   override again.
 *
 *   Zen core only overrides one theme function. If you wish to override it, you
 *   should first look at how Zen core implements this function:
 *     theme_breadcrumbs()      in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called theme hook suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node--forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and theme hook suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440 and http://drupal.org/node/1089656
 */

function ada_theme(&$existing, $type, $theme, $path){
  return array(
    // The form ID.
    'user_login' => array(
      // Forms always take the form argument.
      'render element' => 'form',
      'template' => 'templates/user_login',
    ),

    'user_pass' => array(
      // Forms always take the form argument.
      'render element' => 'form',
      'template' => 'templates/user_pass',
    ),

  );
}

function ada_preprocess_user_login(&$variables) {

  $variables['form']['pass']['#description'] = t('Enter the password that accompanies your username.');

  $variables['forgot_password_title'] = t('Forgot password?');
  $variables['forgot_password_body'] = t('No problem, <a href="@click">click here</a> to get a new password.', array('@click' => '/user/password'));

  $variables['rendered'] = drupal_render_children($variables['form']);

}


function ada_preprocess_user_pass(&$variables) {

  //@TODO Mover a una variable de configuración del theme
  drupal_set_title(t('Log in'));

  //@TODO Mover a una variable de configuración del theme
  $variables['forgot_password_help'] = t('Help text');

  $variables['rendered'] = drupal_render_children($variables['form']);

}

/*
 * Añadimos en la cabecera de las páginas la librería de superfish
 * */

function ada_preprocess_page(&$vars) {

  //Check to see that the user is not logged in and page is /user, redirect the correct page /user/login.
  if ($vars['user']->uid == 0 && arg(0) == 'user' && !arg(1)) {
    drupal_goto("user/login");
  }

  if (module_exists('libraries')) {
    $sf_library = variable_get('superfish_slp',
        libraries_get_path('superfish') . "/jquery.hoverIntent.minified.js\n" .
        libraries_get_path('superfish') . "/jquery.bgiframe.min.js\n" .
        libraries_get_path('superfish') . "/superfish.js\n" .
        libraries_get_path('superfish') . "/supersubs.js\n" .
        libraries_get_path('superfish') . "/supposition.js\n" .
        libraries_get_path('superfish') . "/sftouchscreen.js");
    $sf_library = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", trim($sf_library));
    $sf_library = explode("\n", $sf_library);
    foreach ($sf_library as $s) {
      drupal_add_js($s, 'file');
    }
    drupal_add_css(libraries_get_path('superfish') . '/css/superfish.css', 'file');
    drupal_add_css(libraries_get_path('superfish') . '/css/superfish-vertical.css', 'file');
    drupal_add_css(libraries_get_path('superfish') . '/css/superfish-navbar.css', 'file');
  }

  //dpm($vars);
}

/*
 * Función con la cual reescribimos la salida de los link de menú.
 * */
function ada_menu_link(array $variables){
  //dpm(strpos($variables['element']['#original_link']['menu_name'] , 'nvdm-services-gid-'));
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  //Si el elemento es del menú principal modificamos la salida HTML

  if($variables['element']['#original_link']['menu_name'] == 'main-menu'){

    //añadimos la clase icon-main-menu (esto se podría añadir por configuración en el menú)
    $element['#localized_options']['attributes']['class'][] = 'icon-main-menu';

    /* Activamos el HTML por defecto y cambiamos la salida para el formato
     * <li><a><span>icono</span><span>texto</span></a></li>
     */
    $output = l('<span '.drupal_attributes($element['#localized_options']['attributes']).'></span><span class="title-main-menu">'.$element['#title'].'</span>', $element['#href'], array('html' => TRUE));
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

  }else if($variables['element']['#original_link']['menu_name'] == 'secondary-menu'){

  }else if(strpos($variables['element']['#original_link']['menu_name'] , 'nvdm-services-gid-') !== FALSE){
    //añadimos la clase icon-main-menu (esto se podría añadir por configuración en el menú)
    $element['#localized_options']['attributes']['class'][] = 'item';

    /* Activamos el HTML por defecto y cambiamos la salida para el formato
     * <li><a><span>icono</span><span>texto</span></a></li>
     */
    $output = l('<span '.drupal_attributes($element['#localized_options']['attributes']).'></span><span class="title-menu">'.$element['#title'].'</span>', $element['#href'], array('html' => TRUE));
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

  }

  else{
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }

}

/*
 * En esta función preprocesamos la función de link del menú secundario
 * Para hacerlo más eficiente deberíamos hacer un módulo o bien
 * preprocesar la variable $secondary_links
 * */

function ada_links__system_secondary_menu($variables){
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    //$output = '';

    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,

          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    //Integramos superfish. Añadimos la clase sf-menu a los hijos para poder hacer el desplegable
    $attributes['class'][] = 'sf-menu';
    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    global $user;
    $output .= '<li class="dropdown">';
    $output .= '<a class="btn"><span class="icon-off dropdown"></span><span class="name-user-nav dropdown">'.t('@username', array('@username' => format_username($user))).'</span></a>';

    $output .= '<ul>';

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page())) && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= "</ul>\n";
    $output .= "</li>\n";
    $output .= '</ul>';
  }

  return $output;

}
