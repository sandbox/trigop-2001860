<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 *  * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region, below the main menu (if any).
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 *
 * Title
 * <?php if ($title): ?>
 *    <h1 class="title" id="page-title"><?php print $title; ?></h1>
 *  <?php endif; ?>
 *
 */
?>

<div class="user-login-form-wrapper">
  <?php print $rendered; ?>

  <div class='user-login-forgot'>
    <h2 class='title-forgot-password title'>
      <?php print $forgot_password_title; ?>
    </h2>

    <div class='body-forgot-password'>
      <?php print $forgot_password_body; ?>
    </div>
  </div>

</div>
