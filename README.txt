THEME ADA
----------------------

Integración con menu_attributes para la inclusión de iconos en los menús.
Integración con block_menu para generar los bloques de menú de la columna de la izquierda.

- Menú secundario: Menú user. Utilizar clase para los iconos. Si los iconos no tienen ícono no saldrán.